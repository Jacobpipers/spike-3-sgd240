# Spike Report

## SPIKE 3 - BLUEPRINT BASICS

### Introduction

In Unreal Engine, a lot of work is completed using Blueprints, a form of Visual Programming/Scripting.
We have not previously been exposed to such a system.

### Goals

1.	In a new Blueprint FPS Unreal Engine project, create a “Launch-pad” actor, which can be placed to cause the player to leap into the air upon stepping on it.
	1.	Play a sound when the character is launched
	2.	Set the Launch Velocity using a variable, which lets you place multiple launch-pads with different velocities in the same level
	3.	Add an Arrow Component, and use its rotation to define the direction to launch the character
2.	Create a new level using the default elements available to you from the Unreal Engine (boxes should suffice), which should be a fun little FPS level with many launch-pads that help you access different areas.



### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* https://docs.unrealengine.com/latest/INT/Engine/Blueprints/index.html
* https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html
* https://docs.unrealengine.com/latest/INT/Gameplay/ClassCreation/index.html
* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Follow the Unreal Blueprint [Quicstart Guide](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html)

2. Add Audio to launchpad blueprint after character is launhed. [Play Sound At Location](https://docs.unrealengine.com/latest/INT/BlueprintAPI/Audio/PlaySoundatLocation/index.html)

3. Add the Arrow Component to the Launchpad Blueprint. 
	1. Add direction component to the [event graph](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/Editor/index.html) 
	2. From the direction add new node [Get Forward Vector](https://docs.unrealengine.com/latest/INT/BlueprintAPI/Utilities/Transformation/GetForwardVector/index.html)
	3. In Variables under My Blueprint add variable called Power and set as float. And set as editable. 
	4. From the Get Forward Vector return value add new node multiply vector by float.
	5. Add Power variable to event graph and pin to vector multiply.
	6. Pin the result of the vector multiply to the Launch Character launch velocity pin.

4. Rotatate Arrow to face 90 degrees this can be changed for each instance.


### What we found out

We Found out how to create bluprints.

We Found out how assets like sound can be easily refrenced and changed in a blueprint.



### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.